from pprint import pprint

import requests

from Scripts.utils import (
    script_data,
    timer,
    url_extractor,
    urls,
)


def get_html(url) -> str:
    return requests.get(url).content


def parse_url(url: str):
    html = get_html(url)
    data = script_data(html)
    return url_extractor(data)


def main():
    res = [parse_url(url) for url in urls]
    pprint(res)


if __name__ == "__main__":
    print(timer(main))
