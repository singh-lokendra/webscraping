import json
import re
import time
import urllib.parse
from types import SimpleNamespace

import bs4


def timer(fun):
    start = time.time()
    fun()
    end = time.time()
    return end - start


def url_extractor(data: str):
    """
    Extract useful urls from js script
    Doesn't extract email

    :param data: script data
    """

    # Extraction logic
    json_object = json.loads(data, object_hook=lambda d: SimpleNamespace(**d))
    about = json_object.contents.twoColumnBrowseResultsRenderer.tabs[5].tabRenderer
    items = about.content.sectionListRenderer.contents[0].itemSectionRenderer
    links = items.contents[0].channelAboutFullMetadataRenderer
    urls = [link.navigationEndpoint.urlEndpoint.url for link in links.primaryLinks]

    # Remove the redirects from the url
    clean_urls = []
    for url in urls:
        start = url.find("https%3A%2F%2F")
        if start == -1:
            start = url.find("http%3A%2F%2F")
        link = url[start:]
        readable_url = urllib.parse.unquote(link)
        clean_urls.append(readable_url)

    return clean_urls


def script_data(data) -> str:
    """
    Extracts script data from webpage

    :param url: youtube url to be scraped
    """
    # soup1 = bs4.BeautifulSoup(data, 'html.parser')
    soup1 = bs4.BeautifulSoup(data, "lxml")

    for script in soup1.find_all("script"):
        result: bs4.element.Script = script.find(string=re.compile("var ytInitialData"))

        if result:
            data = str(result)
            pattern = "var ytInitialData = "
            start = data.find(pattern) + len(pattern)
            end = data.find(";</script>", start)
            return data[start:end]


urls = [
    "https://www.youtube.com/c/MostlySane/about",
    "https://www.youtube.com/c/TheChernoProject/about",
    "https://www.youtube.com/c/VivekMuralidharan/about",
    "https://www.youtube.com/c/AakashGupta/about",
    "https://www.youtube.com/c/SaregamaMusic/about",
    "https://www.youtube.com/c/AddictedA1/about",
]
